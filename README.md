# wordpress

in this repo we are using the Dockerfile to create our WordPress image which is based on the `apache-php` image and we are installing some necessary packages alongside the wordpress application.

The image is downloading the latest version of the WordPress everytime. 

Other than that we are passing some `.ini` for the PHP that WordPress recommends.
The first file is the `error-logging.ini` file located in the `etc/php.d/` directory and the configuration is based on the [official WordPress recommendations](https://wordpress.org/support/article/editing-wp-config-php/#configure-error-logging) and the [official WordPress Docker Hub image](https://github.com/docker-library/wordpress/blob/394c79eafd5b345514f87cec590577e641c030ef/latest/php7.4/apache/Dockerfile).
The second file is the `opcache-recommended.ini` file located in the `/etc/php.d/` directory and the configuration is based on the [official PHP recommendations](https://www.php.net/manual/en/opcache.installation.php) and the [official WordPress Docker Hub image](https://github.com/docker-library/wordpress/blob/394c79eafd5b345514f87cec590577e641c030ef/latest/php7.4/apache/Dockerfile).

The core of that image though is the `docker-compose.yml` where we are connecting our database `mariadb` and the `wordpress` images we are creating.

We should mention that with the current configuration we are exporting host's port 8080 to the container's port 80. So if you would like to use another port you should change it from the `docker-compose.yml`.
## How to use

To use that app we first need to create the image from the Dockerfile with the command below

    docker built -t WORDPRESS_NAME_HERE . 

It is important to change the image name on the docker-compose accordingly because right now the image is being called `wordpress:7-christos`.

Another important file to use the `docker-compose.yml` is to create `.env` file. This file will contain the necessary environment variables that we need to install our application. 

We create the file and add the below configuration

    MYSQL_ROOT_PASSWORD=
    MYSQL_USER=
    MYSQL_DATABASE=
    MYSQL_PASSWORD=
    DB_HOST=

- `MYSQL_ROOT_PASSWORD` is as the name says the root password for our database.
- `MYSQL_USER` is the user that we are going to create that will use the wordpress application. We can name it how ever we want to and a common name that is being passed is wordpress
- `MYSQL_DATABASE` is the database that we want to create that our user will have access to. We can name our database how ever we want to.
- `MYSQL_PASSWORD` is the password for the database user that we are creating.
- `DB_HOST` that is based on the name of the container. If we use the default settings the this should be name mariadband it is dependent from the db **container_name** in the `docker-compose.yml`.

If everything is set we can simply type 

    docker-compose up -d

## Troubleshooting 

In case that access to database is not possible from wordpress then make sure that 

- `.env` file is created properly with the above configuration
- Check if volumes `wordpress` and `database` exist already. If they do they
  have probably other credentials. If remembering them or finding them is not
possible then we have to remove these certain volumes.

    docker volume rm wordpress database 

## How to use maintenance.sh and backup.sh scripts

The `backup.sh` script is a script that can backup any docker volume we want. 

It shows the available docker volumes and we decide which one do wewant to backup. To run the script we can simply type:

    ./backup.sh

The format of the named tar files is "DOCKER_VOLUME"-backup-"DAY"-"MONTH"-"YEAR".tar.gz


The `maintenance.sh` script is a script that we can use to backup our application (both wordpress and database), update our WordPress application into the latest version (it is backing up before we update it to ensure that we won't lose any data) and we can use it to import our backed up files into our application in case a version is broken or recover into previous versions.

To use it we can type 

    ./maintenance.sh --help 

or 

    ./maintenance.sh -h

That way we can be informed how to use it's features. 

