<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'database_name_here' );

/** MySQL database username */
define( 'DB_USER', 'username_here' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password_here' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '|S&+o0gC:,NcS!`b6Qe@1U!>88C:_=VR3o`G3gc-?nxLMjPIw+@b;%/iw8EOKDv%' );
define( 'SECURE_AUTH_KEY',  'eguPPS0dV6+sF9OQpFx>Jl+S+`D$O|-RF% vVz|6jJx2s$&t|WD|1bYm/x!}hLQC' );
define( 'LOGGED_IN_KEY',    '-cuk*0+>{4sM.ZDG: yj].t3_yH/4G#|a~EuCy+R6iuvz<+3`.sl-D%KNdEDe6|L' );
define( 'NONCE_KEY',        'ur@OfxvrIJ&n[!ddq2-|1[&+lN9X]bq?U]@iWS~NFRAi@oe2,X8&Pmee(|E8EY%K' );
define( 'AUTH_SALT',        '*O}2opcS8c5oCM4IG?~wvQ>y.W*fm&(N<w?2&1)+=b@|NUEq783Y^ICqLlkn)H_,' );
define( 'SECURE_AUTH_SALT', 'a=g!W=?03+W31KI+9>cqA]RbD#u$NwV^kSYBs<tfyx#-ipSpy@m4kA2hPy@eLg2A' );
define( 'LOGGED_IN_SALT',   '8<Ht/Bg!BkX^SVB!.aRs|~u|_%m)w{|p|T*+5]rupn;7-oCQ-z}eDl+2d?SB|@%b' );
define( 'NONCE_SALT',       'Xo% 7hS|d&>3Jvh.2+fm1u)/^$IUVQ;y&<,jk=@e*G-,&.$YJgD;(w#~P2yo!6)-' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
