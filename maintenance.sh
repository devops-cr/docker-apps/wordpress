#!/bin/sh

# This is a one line command that we can find every time
# the path that the script is placed to.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

WORDPRESS_VOLUME=wordpress
DB_VOLUME=database

# COLORS
RED="\e[31m"
GREEN="\e[32m"
RESET="\e[0m"

how_to(){
	
	echo -e "
	
	This script is being used to update an outdated wordpress image
	if access to the admin page is not possible.

	It has four basic commands where we can update our docker container
	,import a backup ".tar.gz" file or backup the WordPress Application and
       	mariadb database that we created.

	${GREEN}Usage:${RESET}

	${GREEN} $0 upgrade${RESET}
	  This command is backing up both the database and the wordpress application

	${GREEN} $0 import wordpress FILE_PATH${RESET}
	  This command imports a tar file to the database volume and it requires 
	  an argument to pass the path of the tar file otherwise it will not work.
	  The path can be either a relative path or an absolute path.
	
	${GREEN} $0 import database FILE_PATH${RESET}
	  This command imports a tar file to the database volume and it requires 
	  an argument to pass the path of the tar file otherwise it will not work.
	  The path can be either a relative path or an absolute path.

	${GREEN} $0 backup${RESET}
	  This command executes only a backup of the database. The backup files
	  are located in the backup/database and backup/wordpress directories and their naming are 
	  database-backup-DAY-MONTH-YEAR.tar.gz
	  wordpress-backup-DAY-MONTH-YEAR.tar.gz

	"

}




backup(){
	WORDPRESS_BACKUP_FILE=${WORDPRESS_VOLUME}-backup-$(date +%d-%m-%y).tar.gz
	WORDPRESS_INSPECT_VOLUME=$(docker volume inspect ${WORDPRESS_VOLUME} 2>&1)

	DB_BACKUP_FILE=${DB_VOLUME}-backup-$(date +%d-%m-%y).tar.gz
	DB_INSPECT_VOLUME=$(docker volume inspect ${DB_VOLUME} 2>&1)

	DB_BACKUP_DIR=${DIR}/backup/database
	WORDPRESS_BACKUP_DIR=${DIR}/backup/wordpress
	mkdir -p ${DB_BACKUP_DIR}
	mkdir -p ${WORDPRESS_BACKUP_DIR}
	echo " 
	Backing up docker volumes ${WORDPRESS_VOLUME} and ${DB_VOLUME} before upgrade wordpress
	"

	# The way that this backup is working is that we are creating a docker container from busybox
	# and we are passing the volumes we want to backup and another directory in our file system.
	# That way we are making a tar from the wordpress volume and we are sending it to the file system
	# directory.
	docker run --rm -v ${WORDPRESS_VOLUME}:/${WORDPRESS_VOLUME}-backup \
		   -v ${WORDPRESS_BACKUP_DIR}:/backup \
		   busybox \
		   tar cfz /backup/${WORDPRESS_BACKUP_FILE} /${WORDPRESS_VOLUME}-backup

	docker run --rm -v ${DB_VOLUME}:/${DB_VOLUME}-backup \
		   -v ${DB_BACKUP_DIR}:/backup \
		   busybox \
		   tar cfz /backup/${DB_BACKUP_FILE} /${DB_VOLUME}-backup

	echo ' 
	Succesfully backed up the volumes 
	'
}

# We are checking the import type because we want it to be 
# Either 'wordpress' or 'database'. After our checks if everything
# is fine then we are importing the desired tar file. we are using 
# a docker run command as and intermediate to connect our volumes
# together because they are not accessible otherwise since we are not 
# using filesystem volumes but docker volumes in /var/lib/docker/volumes.
import() {
	IMPORT_TYPE="$2"
	TAR_FILE="$3"
	if [ -z "$IMPORT_TYPE" ]; then
		echo -e "
		${RED}Error:${RESET} not enough arguments
		You have to define if you want to import 
		wordpress or database
		"
		how_to
		exit 1
	fi
	
	if [ "$IMPORT_TYPE" == "wordpress" ]; then
	
		 if [ -z "$TAR_FILE" ]; then
			echo -e " 
			${RED}Error:${RESET} Not enough arguments
			You have to pass a tar file name "
			how_to
			exit 1
		fi
		if [ ! -f "$TAR_FILE" ]; then 
			echo -e " 
			${RED}Error:${RESET} The file does not exist "
			exit 1
		fi
		if [ "$#" -ne 3 ]; then
			echo -e "
			${RED}Error:${RESET} Too many arguments"
			how_to
			exit 1
		fi
		
		echo '
		Importing WordPress Application
		'	
		IMPORT_DIR=${DIR}/import
		rm -rf ${IMPORT_DIR}
		mkdir -p ${IMPORT_DIR}
		tar xfz ${TAR_FILE} && mv wordpress-backup ${IMPORT_DIR}
		rm -rf ${IMPORT_DIR}/wordpress-backup/wp-content
	
		# We are creating a user and a group apache with ID 48 because that's the default ID that apache uses 
		# That way we can change the ownership to apache:apache and make sure that these changes apply to 
		# our desired host as well
		docker run --rm \
			   -v ${WORDPRESS_VOLUME}:/wordpress \
			   -v ${IMPORT_DIR}/wordpress-backup:/import \
			   alpine \
	           	   sh -c "rm -rf /wordpress/wp-admin /wordpress/wp-includes && yes | cp -r /import/. /wordpress/ && addgroup -g 48 apache && adduser -u 48 -D -G apache apache && chown -R apache:apache /wordpress"
		
		rm -rf ${IMPORT_DIR}
		echo '
		The import was successful
		'
	fi

	if [ "$IMPORT_TYPE" == "database" ]; then
	
		if [ -z "$TAR_FILE" ]; then
			echo -e " 
			${RED}Error:${RESET} Not enough arguments
			You have to pass a tar file name 
			"
			how_to
			exit 1
		fi
		if [ ! -f "$TAR_FILE" ]; then 
			echo -e " ${RED}Error:${RESET} The file does not exist "
			exit 1
		fi
		if [ "$#" -ne 3 ]; then
			echo -e "
			${RED}Error:${RESET} Too many arguments"
			how_to
			exit 1
		fi
		
		echo '
		Importing MariaDB Database
		'	
		IMPORT_DIR=${DIR}/import
		rm -rf ${IMPORT_DIR}
		mkdir -p ${IMPORT_DIR}
		tar xfz ${TAR_FILE} && mv database-backup ${IMPORT_DIR}
	
		# We are creating a user and a group db-user with ID 990 because that's the default ID that
		# our docker image mariadb is using and that's where are assigned our database.
		# That way we can change the ownership to apache:apache and make sure that these changes apply to 
		# our desired host as well
		docker run --rm \
			   -v ${DB_VOLUME}:/db \
			   -v ${IMPORT_DIR}/database-backup:/import \
			   alpine \
	           	   sh -c "yes | cp -r /import/. /db/ && addgroup -g 990 db_user && adduser -u 990 -D -G db_user db_user && chown -R db_user:db_user /db"
		
		rm -rf ${IMPORT_DIR}
		echo '
		The import was successful
		'
	fi

	
}

upgrade() {

	echo " Upgrading wordpress to the latest version !"
	UPGRADE_DIR=${DIR}/upgrade
	rm -rf ${UPGRADE_DIR}
	mkdir -p ${UPGRADE_DIR}
	cd ${UPGRADE_DIR} && curl -sSO https://wordpress.org/latest.tar.gz
	tar xfz latest.tar.gz -C ${UPGRADE_DIR} --strip-components=1
	rm -rf ${UPGRADE_DIR}/latest.tar.gz \
	       ${UPGRADE_DIR}/wp-content
	
	# We are creating a user and a group apache with ID 48 because that's the default ID that apache uses 
	# That way we can change the ownership to apache:apache and make sure that these changes apply to 
	# Our desired host as well
	docker run --rm \
	           -v ${WORDPRESS_VOLUME}:/wordpress \
		   -v ${UPGRADE_DIR}:/upgrade \
		   alpine \
	           sh -c "rm -rf /wordpress/wp-admin /wordpress/wp-includes && yes | cp -r /upgrade/. /wordpress/ && addgroup -g 48 apache && adduser -u 48 -D -G apache apache && chown -R apache:apache /wordpress"
	rm -rf ${UPGRADE_DIR}
	echo " Update is completed !"
	
}

if [ -z "$1" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ]; then

	how_to
	exit 0

fi

COMMAND="$1"
case "$COMMAND" in
	upgrade) 
		backup
		upgrade
		;;
	import)
		import "$@"
		;;
	backup) backup
		;;
esac

