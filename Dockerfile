FROM apache-php:7-christos

RUN yum install -y php-xml php-mbstring php-zip wget \
 && yum clean all \
 && wget https://wordpress.org/latest.tar.gz \
 && tar -xzf latest.tar.gz -C /var/www/html/ --strip-components=1 \
 && rm -rf latest.tar.gz \
 && yum remove -y wget


COPY var/ /var/
COPY etc/ /etc/
