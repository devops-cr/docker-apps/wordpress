#!/bin/sh

correct = false
while [ "${correct}" != "true" ]; do 

	docker volume ls
	read -p "What is the volume that you want to backup ?  -->"  DOCKER_VOLUME

	BACKUP_FILE=${DOCKER_VOLUME}-backup-$(date +%d-%m-%y).tar.gz
	DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
 
	INSPECT_VOLUME=$(docker volume inspect ${DOCKER_VOLUME} 2>&1)
	if [[ ${INSPECT_VOLUME} == *"No such volume"* ]]; then
		echo ' 
		       ******************************
		       *                            *
		       * Pass a valid docker volume *
		       *                            *
	 	       ****************************** 
		       '
		
	else 

		correct=true

	fi
done

	mkdir -p ${DIR}/backup
	BACKUP_DIR=${DIR}/backup
	echo " Backing up docker volume ${DOCKER_VOLUME} "
	docker run --rm -v ${DOCKER_VOLUME}:/${DOCKER_VOLUME}-backup \
	  -v ${BACKUP_DIR}:/backup \
	  busybox \
	  tar cfz /backup/${BACKUP_FILE} /${DOCKER_VOLUME}-backup
	echo ' Successfully backed up the volume ' 
	


